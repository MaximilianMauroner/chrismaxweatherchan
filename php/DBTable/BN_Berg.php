<?php

class BN_Berg extends DatabaseTable implements JsonSerializable
{
    private $id;
    private $name;
    private $altitude;
    private $temp;
    private $relativehumidity;
    private $snowheight;
    private $avgwindspeed;
    private $percipitation;
    private $longitude;
    private $latitude;

    public function __construct($data = array())
    {
        if ($data) {
            foreach ($data as $key => $value) {
                $setterName = "set" . ucfirst($key);
                if (method_exists($this, $setterName)) {
                    if (empty($value))
                        $value = null;
                    $this->$setterName($value);
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * @param mixed $altitude
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;
    }

    /**
     * @return mixed
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param mixed $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * @return mixed
     */
    public function getRelativehumidity()
    {
        return $this->relativehumidity;
    }

    /**
     * @param mixed $relativehumidity
     */
    public function setRelativehumidity($relativehumidity)
    {
        $this->relativehumidity = $relativehumidity;
    }

    /**
     * @return mixed
     */
    public function getSnowheight()
    {
        return $this->snowheight;
    }

    /**
     * @param mixed $snowheight
     */
    public function setSnowheight($snowheight)
    {
        $this->snowheight = $snowheight;
    }

    /**
     * @return mixed
     */
    public function getAvgwindspeed()
    {
        return $this->avgwindspeed;
    }

    /**
     * @param mixed $avgwindspeed
     */
    public function setAvgwindspeed($avgwindspeed)
    {
        $this->avgwindspeed = $avgwindspeed;
    }

    /**
     * @return mixed
     */
    public function getPercipitation()
    {
        return $this->percipitation;
    }

    /**
     * @param mixed $percipitation
     */
    public function setPercipitation($percipitation)
    {
        $this->percipitation = $percipitation;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }


    /**
     * @inheritDoc
     */
    public function save()
    {
        if ($this->getId() > 0) {
            $this->_update();
        } else {
            $this->_insert();
        }
    }


    /**
     * @inheritDoc
     */
    protected function _insert()
    {
        $sql = 'INSERT INTO bn_berg (name,altitude, temp,relativehumidity,snowheight,avgwindspeed,precipitation,longitude,latitude)'
            . 'VALUES (:name,:altitude, :temp,:relativehumidity,:snowheight,:avgwindspeed,:precipitation,:longitude,:latitude)';
        $query = Database::getDB()->prepare($sql);
        $query->execute($this->toArray(false));

        // setze die ID auf den von der DB generierten Wert
        $this->id = Database::getDB()->lastInsertId();
    }

    /**
     * updates values in Database
     */
    protected function _update()
    {
        $sql = 'UPDATE bn_berg SET id=:id, name=:name, altitude=:altitude, temp=:temp,relativehumidity=:relativehumidity,snowheight=:snowheight,avgwindspeed=:avgwindspeed,precipitation=:precipitation,longitude=:longitude,latitude=:latitude '
            . 'WHERE id=:id';

        $query = Database::getDB()->prepare($sql);
        $query->execute($this->toArray());
    }


    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

}