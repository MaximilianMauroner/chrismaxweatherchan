import psycopg2


user = "swztlueq",
password = "AdY0LnqhGUE-clB7LvdFFdkrmyqe8X9d",
host = "manny.db.elephantsql.com",
port = "5432",
database = "swztlueq"


def insertTal(records):
    try:
        connection = psycopg2.connect(user="swztlueq",
                                      password="AdY0LnqhGUE-clB7LvdFFdkrmyqe8X9d",
                                      host="manny.db.elephantsql.com",
                                      port="5432",
                                      database="swztlueq")
        cursor = connection.cursor()
        sql_insert_query = """ INSERT INTO bn_tal (name, altitude, temp, relativehumidity, pressure, avgwindspeed, precipitation) VALUES (%s,%s,%s,%s,%s,%s,%s)"""

        # executemany() to insert multiple rows rows
        result = cursor.executemany(sql_insert_query, records)
        connection.commit()
        print(cursor.rowcount, "Record inserted successfully into mobile table")

    except (Exception, psycopg2.Error) as error:
        print("Failed inserting record into bn_tal table {}".format(error))

    finally:
        # closing database connection.
        if (connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


def insertBerg(records):
    try:
        connection = psycopg2.connect(user="swztlueq",
                                      password="AdY0LnqhGUE-clB7LvdFFdkrmyqe8X9d",
                                      host="manny.db.elephantsql.com",
                                      port="5432",
                                      database="swztlueq")
        cursor = connection.cursor()
        sql_insert_query = """ INSERT INTO bn_berg (name, altitude, temp, relativehumidity, snowheight, avgwindspeed, precipitation) VALUES (%s,%s,%s,%s,%s,%s,%s)"""

        # executemany() to insert multiple rows rows
        result = cursor.executemany(sql_insert_query, records)
        connection.commit()
        print(cursor.rowcount, "Record inserted successfully into mobile table")

    except (Exception, psycopg2.Error) as error:
        print("Failed inserting record into bn_tal table {}".format(error))

    finally:
        # closing database connection.
        if (connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


def updateMultipleTal(records):
    global user
    global password
    global host
    global port
    global database
    try:
        ps_connection = psycopg2.connect(user="swztlueq",
                                         password="AdY0LnqhGUE-clB7LvdFFdkrmyqe8X9d",
                                         host="manny.db.elephantsql.com",
                                         port="5432",
                                         database="swztlueq")
        cursor = ps_connection.cursor()
        # UPDATE table   SET column1 = value1,    column2 = value2 ,...
        # Update multiple records
        sql_update_query = """Update bn_tal set altitude = %s,temp = %s, relativehumidity = %s, pressure = %s, avgwindspeed = %s, precipitation = %s  where name = %s"""
        cursor.executemany(sql_update_query, records)
        ps_connection.commit()
        print(records)
        row_count = cursor.rowcount
        print(row_count, "Records Updated")

    except (Exception, psycopg2.Error) as error:
        print("Error while updating PostgreSQL table", error)

    finally:
        # closing database connection.
        if (ps_connection):
            cursor.close()
            ps_connection.close()
            print("PostgreSQL connection is closed")


def updateMultipleBerg(records):
    try:
        ps_connection = psycopg2.connect(user="swztlueq",
                                         password="AdY0LnqhGUE-clB7LvdFFdkrmyqe8X9d",
                                         host="manny.db.elephantsql.com",
                                         port="5432",
                                         database="swztlueq")
        cursor = ps_connection.cursor()
        # UPDATE table   SET column1 = value1,    column2 = value2 ,...
        # Update multiple records
        sql_update_query = """Update bn_berg set altitude = %s,temp = %s, relativehumidity = %s, snowheight = %s, avgwindspeed = %s, precipitation = %s  where name = %s"""
        cursor.executemany(sql_update_query, records)
        ps_connection.commit()

        row_count = cursor.rowcount
        print(row_count, "Records Updated")

    except (Exception, psycopg2.Error) as error:
        print("Error while updating PostgreSQL table", error)

    finally:
        # closing database connection.
        if (ps_connection):
            cursor.close()
            ps_connection.close()
            print("PostgreSQL connection is closed")
