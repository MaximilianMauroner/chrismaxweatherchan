import requests
import json


def getApiValue(url):
    header = requests.post(url)
    header.headers
    # print(header.headers)
    response = requests.get(url)
    rep = response.json()
    return rep


def getAvgMaxMin(rep):
    # print (rep)
    mintemp = float(rep['rows'][0]['t'])
    maxtemp = float(rep['rows'][0]['t'])
    avg = 0.0
    counter = 0
    # bozen = rep['rows'][0]
    for i in rep['rows']:
        cur = float(i['t'])
        avg += cur
        counter += 1
        if cur < mintemp:
            mintemp = cur
        if cur > maxtemp:
            maxtemp = cur
    avg = avg/counter
    return avg


def getAllInfo(rep, ort):
    arr = []
    if ort == "tal":
        j = 0
        for i in rep['rows']:
            temp = []
            temp.append(i['name'])
            temp.append(i['altitude'])
            temp.append(i['t'])
            temp.append(rep['rows'][j]['rh'])
            temp.append(i['p'])
            temp.append(i['ff'])
            temp.append(i['n'])
            arr.append(temp)
            j += 1
    elif ort == "berg":
        j = 0
        for i in rep['rows']:
            temp = []
            temp.append(i['name'])
            temp.append(i['altitude'])
            temp.append(i['t'])
            temp.append(rep['rows'][j]['rh'])
            temp.append(i['hs'])
            temp.append(i['ff'])
            temp.append(i['n'])
            arr.append(temp)
            j += 1
    arr.sort(key=lambda x: x[0])
    return arr


def getAllWeatherInfo(rep):
    print(type(rep))


def initAPI():
    tal = getApiValue(
        'http://daten.buergernetz.bz.it/services/weather/station?categoryId=1&lang=de&format=json')
    berg = getApiValue(
        'http://daten.buergernetz.bz.it/services/weather/station?categoryId=3&lang=de&format=json')
    weatherInfo = getApiValue(
        'http://daten.buergernetz.bz.it/services/weather/bulletin?format=json&lang=de')

    talVal = getAllInfo(tal, "tal")
    bergVal = getAllInfo(berg, "berg")
    return talVal, bergVal


weatherInfo = getApiValue(
    'http://daten.buergernetz.bz.it/services/weather/bulletin?format=json&lang=de')
getAllWeatherInfo(weatherInfo)
