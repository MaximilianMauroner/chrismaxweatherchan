<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>chrismaxweather</title>
    <link rel="stylesheet" href="../CSS/styles.css">
    <script src="https://kit.fontawesome.com/6119e20d35.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>

<body style="background-color: lightgray">
<?php
include 'php/Database.php';
include 'php/DBTable/DatabaseTable.php';
include 'php/DBTable/BN_Tal.php';

function getInfoTal($rep)
{
    $temp = array();
    foreach ($rep['rows'] as $key => $i) {
        $temp[$key]['name'] = $i['name'];
        $temp[$key]['altitude'] = $i['altitude'];
        $temp[$key]['temp'] = $i['t'];
        $temp[$key]['relativehumidity'] = $rep['rows'][$key]['rh'];
        $temp[$key]['pressure'] = $i['p'];
        $temp[$key]['avgwindspeed'] = $i['ff'];
        $temp[$key]['precipitation'] = $i['n'];
        $temp[$key]['longitude'] = $i['longitude'];
        $temp[$key]['latitude'] = $i['latitude'];
    }
    return $temp;
}

$url = "http://daten.buergernetz.bz.it/services/weather/station?categoryId=1&lang=de&format=json";
$json = file_get_contents($url);
$arr = json_decode($json, true);
?>
<header>
    <div id="logo">
        <figure>
            <img alt="logo" src="../img/logo/weather_logo.png"/>
            <figcaption></figcaption>
        </figure>
    </div>
    <nav>
        <ul>
            <li>
                <a href="#">
                    <i class="fas fa-bars"></i>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-cloud"></i>
                </a>
            </li>
            <a href="#">
                <i></i>
            </a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <div id="title">
        <h1 id="welcome">Welcome to our Garbage-Page!</h1>
    </div>
    <p id="location"></p>
    <p id="nearest"></p>
    <img src="http://daten.buergernetz.bz.it/services/weather/graphics/charts/1292/2/de?scat_id=1" alt="Luftdruck" id="luftdruck" />
    <img src="" alt="Luftdruck"  />

        <script>
            let arr_data = "";
            var lat;
            var long;
            window.onload = getLocation();
            var locationp = document.getElementById("location");

            function getLocation() {
                navigator.geolocation.getCurrentPosition(showPosition1);
                if (typeof lat === "undefined" || typeof long === "undefined") {
                    console.log("We cannot get the geolocation (too fast? user/browser blocked it?)");

                }
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);

                } else {
                    locationp.innerHTML = "Geolocation is not supported by this browser.";
                    return
                }
            }

            function showPosition1(position) {
                lat = position.coords.latitude;
                long = position.coords.longitude;
            }

            function showPosition(position) {
                lat = position.coords.latitude;
                long = position.coords.longitude;
                locationp.innerHTML = "Latitude: " + lat + "<br>Longitude: " + long;
                nearest();
            }

            function distance(lat1, lon1, lat3, lon3) {
                lon3 = lon3.replace(",", ".");
                lat3 = lat3.replace(",", ".");
                var lat2 = parseFloat(lat3, 10);
                var lon2 = parseFloat(lon3);
                var tempx = lat2 - lat1;
                var tempy = lon2 - lon1;
                var x = Math.abs(tempx);
                var y = Math.abs(tempy);
                x = x * x;
                y = y * y;
                var distance = Math.sqrt(x + y);
                return distance;
            }

            function close(arr) {
                var z = arr.sort((a, b) => a.distance - b.distance);
                return z[0];
            }

            var distancep = document.getElementById("nearest");


            function nearest() {
                let ar = <?php echo json_encode($arr); ?>;
                let array = ar;
                var a = [];
                console.log(typeof ar);
                for (var i = 0; i < ar.rows.length; i++) {
                    console.log(ar.rows[i].name, ar.rows[i].latitude, ar.rows[i].longitude);
                    var temp = {
                        distance: distance(lat, long, ar.rows[i].latitude, ar.rows[i].longitude),
                        name: ar.rows[i].name
                    }

                    a.push(temp);

                }
                let closest = close(a);
                distancep.innerHTML = "<br>Distance: " + closest.distance + "<br>Name: " + closest.name;
                arr_data = findElement(array, closest.name); // x is arr + name
                console.log(arr_data); // displays data
                let image = document.getElementById("luftdruck");
                image.src = arr_data.Luftdruck;
            }

            function findElement(array, propValue) {
                let garbage = "";
                for (let i = 0; i < array.rows.length; i++) {
                    if (array.rows[i].name == propValue) {
                        if(array.rows[i].measurements.length > 4 ) {
                            garbage = {
                                altitude: array.rows[i].altitude,
                                categoryId: array.rows[i].categoryId,
                                code: array.rows[i].code,
                                dd: array.rows[i].dd,
                                ff: array.rows[i].ff,
                                hs: array.rows[i].hs,
                                id: array.rows[i].id,
                                lastUpdated: array.rows[i].lastUpdated,
                                latitude: array.rows[i].latitude,
                                longitude: array.rows[i].longitude,
                                lwdType: array.rows[i].lwdType,
                                n: array.rows[i].n,
                                name: array.rows[i].name,
                                p: array.rows[i].p,
                                q: array.rows[i].q,
                                rh: array.rows[i].rh,
                                t: array.rows[i].t,
                                vaxcode: array.rows[i].vaxcode,
                                w: array.rows[i].w,
                                wMax: array.rows[i].wMax,
                                sd: array.rows[i].sd,
                                gs: array.rows[i].gs,
                                wt: array.rows[i].wt,
                                visibility: array.rows[i].visibility,
                                zoomLevel: array.rows[i].zoomLevel,
                                Lufttemperatur: array.rows[i].measurements[0].imageUrl,
                                Niederschlag: array.rows[i].measurements[1].imageUrl,
                                Windrichtung: array.rows[i].measurements[2].imageUrl,
                                Windgeschwindigkeit: array.rows[i].measurements[3].imageUrl,
                                relative_Luftfeuchte: array.rows[i].measurements[4].imageUrl,
                                Luftdruck: array.rows[i].measurements[5].imageUrl,
                                Globalstrahlung: array.rows[i].measurements[6].imageUrl,
                                Sonnenscheindauer: array.rows[i].measurements[7].imageUrl,
                            }
                            }else{
                                 garbage = {
                                    altitude: array.rows[i].altitude,
                                    categoryId: array.rows[i].categoryId,
                                    code: array.rows[i].code,
                                    dd: array.rows[i].dd,
                                    ff: array.rows[i].ff,
                                    hs: array.rows[i].hs,
                                    id: array.rows[i].id,
                                    lastUpdated: array.rows[i].lastUpdated,
                                    latitude: array.rows[i].latitude,
                                    longitude: array.rows[i].longitude,
                                    lwdType: array.rows[i].lwdType,
                                    n: array.rows[i].n,
                                    name: array.rows[i].name,
                                    p: array.rows[i].p,
                                    q: array.rows[i].q,
                                    rh: array.rows[i].rh,
                                    t: array.rows[i].t,
                                    vaxcode: array.rows[i].vaxcode,
                                    w: array.rows[i].w,
                                    wMax: array.rows[i].wMax,
                                    sd: array.rows[i].sd,
                                    gs: array.rows[i].gs,
                                    wt: array.rows[i].wt,
                                    visibility: array.rows[i].visibility,
                                    zoomLevel: array.rows[i].zoomLevel,
                                    Lufttemperatur: array.rows[i].measurements[0].imageUrl,
                                    Niederschlag: array.rows[i].measurements[1].imageUrl,
                                    relative_Luftfeuchte: array.rows[i].measurements[2].imageUrl,
                                    Luftdruck: array.rows[i].measurements[3].imageUrl
                                    }


                        }
                        return garbage;
                    }

                }
            }

    </script>
</main>
<footer>
    <p id="copyright">
        Copyright &copy; Max and Chris
    </p>
</footer>
</body>

</html>